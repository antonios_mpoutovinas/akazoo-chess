package com.anmpout.chessknight;

import android.app.Application;
import android.content.Context;

/**
 * Created by anmpout on 04/11/2019
 */
public class ChessKnightApplication extends Application {
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}
