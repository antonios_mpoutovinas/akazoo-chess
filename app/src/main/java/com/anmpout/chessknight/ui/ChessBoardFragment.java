package com.anmpout.chessknight.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.chessknight.R;
import com.anmpout.chessknight.databinding.FragmentChessBoardBinding;
import com.anmpout.chessknight.model.BoardBlock;
import com.anmpout.chessknight.model.Pos;
import com.anmpout.chessknight.utils.Utils;
import com.anmpout.chessknight.utils.VerticalSpaceItemDecoration;
import com.anmpout.chessknight.viewmodel.ChessBoardViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anmpout on 04/11/2019
 */
public class ChessBoardFragment extends Fragment implements ChessBoardAdapter.ActionListener {
    private static String TAG = ChessBoardFragment.class.getCanonicalName();

    private ChessBoardViewModel viewModel;
    private RecyclerView mRecyclerView;
    private ChessBoardAdapter adapter;
    private RecyclerView resultsRecyclerView;
    FragmentChessBoardBinding binding;
    private ResultsAdapter resultsAdapter;

    public ChessBoardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentChessBoardBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chess_board, container, false);
        View rootView = binding.getRoot();
        binding.setLifecycleOwner(this);
        this.binding = binding;
        initView();

        return rootView;
    }

    private void initView() {
        mRecyclerView = binding.chessBoardRecyclerView;
        adapter = new ChessBoardAdapter();
        adapter.setActionListener(this);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 8));
        resultsRecyclerView = binding.resultRecyclerView;
        resultsAdapter = new ResultsAdapter();
        resultsRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(Utils.dp(getActivity(), 6)));
        resultsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
        resultsRecyclerView.setAdapter(resultsAdapter);
        binding.setViewModel(viewModel);
        binding.btnExecute.setEnabled(false);
        binding.btnExecute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultsAdapter.setResultSet(new ArrayList<>());
                BoardBlock start = adapter.getSelected().get(0);
                BoardBlock end = adapter.getSelected().get(1);
                Log.d(TAG, start.toString());
                Log.d(TAG, end.toString());
                viewModel.calculateSolution(new Pos(start.getRow(), start.getColumn(), 0), new Pos(end.getRow(), end.getColumn(), Integer.MAX_VALUE), 8);
            }
        });
        binding.btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultsAdapter.setResultSet(new ArrayList<>());
                adapter.setSelected(new ArrayList<>());
                viewModel.fetchChessBoard(8);
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureViewModel();
    }


    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this).get(ChessBoardViewModel.class);
        viewModel.getChessBoardBlocks().observe(this, this::updateUI);
        viewModel.getResultSet().observe(this, this::setResults);
    }

    private void setResults(List<Pos> pos) {
        if (pos == null || pos.isEmpty()) {
            Toast.makeText(getActivity(), "Can't solve this problem", Toast.LENGTH_LONG).show();

        } else if (pos.size() > 4) {
            Toast.makeText(getActivity(), "Can't reach the ending position in 3 moves", Toast.LENGTH_LONG).show();
        } else {
            resultsAdapter.setResultSet(pos);
        }
    }

    private void updateUI(List<BoardBlock> boardBlocks) {
        if (boardBlocks != null && !boardBlocks.isEmpty()) {
            adapter.setBlockList(boardBlocks);
            adapter.setSelected(new ArrayList<>());
        }

    }

    @Override
    public void enableExecuteButton(boolean enable) {
        if (binding.btnExecute.isEnabled() != enable && enable) {
            Toast.makeText(getActivity(), "You can try solve it!", Toast.LENGTH_LONG).show();
        }
        binding.btnExecute.setEnabled(enable);
    }
}
