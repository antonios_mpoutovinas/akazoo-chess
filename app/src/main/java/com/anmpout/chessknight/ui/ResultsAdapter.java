package com.anmpout.chessknight.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.chessknight.R;
import com.anmpout.chessknight.databinding.RowResultBinding;
import com.anmpout.chessknight.model.Pos;

import java.util.List;

/**
 * Created by anmpout on 05/11/2019
 */
public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.ResultViewHolder> {

    private List<Pos> resultSet;

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowResultBinding resultBinding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                R.layout.row_result, viewGroup, false);
        return new ResultViewHolder(resultBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder profileViewHolder, int i) {
        Pos result = resultSet.get(i);
        profileViewHolder.resultBinding.setResult(result);
    }

    @Override
    public int getItemCount() {
        if (resultSet != null) {
            return resultSet.size();
        } else {
            return 0;
        }
    }

    public void setResultSet(List<Pos> resultSet) {
        this.resultSet = resultSet;
        notifyDataSetChanged();
    }


    class ResultViewHolder extends RecyclerView.ViewHolder {

        private RowResultBinding resultBinding;

        public ResultViewHolder(@NonNull final RowResultBinding resultBinding) {
            super(resultBinding.getRoot());

            this.resultBinding = resultBinding;
        }

    }
}