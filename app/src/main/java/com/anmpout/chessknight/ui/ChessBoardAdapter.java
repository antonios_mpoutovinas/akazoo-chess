package com.anmpout.chessknight.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.chessknight.R;
import com.anmpout.chessknight.databinding.RowChessBlockBinding;
import com.anmpout.chessknight.model.BoardBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anmpout on 04/11/2019
 */
public class ChessBoardAdapter extends RecyclerView.Adapter<ChessBoardAdapter.BlockViewHolder> {

    private List<BoardBlock> blocks;
    private ActionListener actionListener;
    private List<BoardBlock> selected = new ArrayList<>();

    @NonNull
    @Override
    public BlockViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowChessBlockBinding rowChessBlockBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.row_chess_block, viewGroup, false);
        return new BlockViewHolder(rowChessBlockBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BlockViewHolder profileViewHolder, int i) {
        BoardBlock block = blocks.get(i);
        profileViewHolder.blockListItemBinding.setBlock(block);
    }

    @Override
    public int getItemCount() {
        if (blocks != null) {
            return blocks.size();
        } else {
            return 0;
        }
    }

    public void setBlockList(List<BoardBlock> blocks) {
        this.blocks = blocks;
        notifyDataSetChanged();
    }

    public ActionListener getActionListener() {
        return actionListener;
    }

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public List<BoardBlock> getSelected() {
        return selected;
    }

    public void setSelected(List<BoardBlock> selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    class BlockViewHolder extends RecyclerView.ViewHolder {

        private RowChessBlockBinding blockListItemBinding;

        public BlockViewHolder(@NonNull final RowChessBlockBinding blockListItemBinding) {
            super(blockListItemBinding.getRoot());
            blockListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (actionListener != null) {
                        if (blockListItemBinding.getBlock().getSelected() == BoardBlock.NOT_SELECTED && selected.isEmpty()) {
                            blockListItemBinding.getBlock().setSelected(BoardBlock.START_SELECTED);
                            selected.add(blockListItemBinding.getBlock());
                            notifyDataSetChanged();
                        } else if (blockListItemBinding.getBlock().getSelected() == BoardBlock.START_SELECTED && selected.size() == 1) {
                            blockListItemBinding.getBlock().setSelected(BoardBlock.NOT_SELECTED);
                            selected.remove(blockListItemBinding.getBlock());
                            notifyDataSetChanged();
                        } else if (blockListItemBinding.getBlock().getSelected() == BoardBlock.NOT_SELECTED && selected.size() == 1) {
                            blockListItemBinding.getBlock().setSelected(BoardBlock.END_SELECTED);
                            selected.add(blockListItemBinding.getBlock());
                            notifyDataSetChanged();
                        } else if (blockListItemBinding.getBlock().getSelected() == BoardBlock.END_SELECTED && selected.size() == 2) {
                            selected.remove(blockListItemBinding.getBlock());
                            blockListItemBinding.getBlock().setSelected(BoardBlock.NOT_SELECTED);
                            notifyDataSetChanged();
                        }
                        actionListener.enableExecuteButton(selected.size() == 2);
                    }

                }
            });
            this.blockListItemBinding = blockListItemBinding;
        }


    }

    /**
     * The interface Action listener.
     */
    interface ActionListener {
        /**
         * Enables button when list is ready.
         *
         * @param enable the enable value
         */
        void enableExecuteButton(boolean enable);


    }

}