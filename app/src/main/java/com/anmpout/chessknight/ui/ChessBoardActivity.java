package com.anmpout.chessknight.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.anmpout.chessknight.R;

public class ChessBoardActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.showFragment(savedInstanceState);
        this.initializeActionBar(getResources().getString(R.string.app_name));
    }

    private void showFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {

            ChessBoardFragment fragment = new ChessBoardFragment();
            Bundle bundle = new Bundle();
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.content_frame, fragment, null)
                    .commit();
        }
    }

    /**
     * set the title to action bar
     */
    public void initializeActionBar(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

    }
}
