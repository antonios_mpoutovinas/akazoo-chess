package com.anmpout.chessknight.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.anmpout.chessknight.engine.KnightGameEngine;
import com.anmpout.chessknight.model.BoardBlock;
import com.anmpout.chessknight.model.BoardFactory;
import com.anmpout.chessknight.model.Pos;

import java.util.List;

/**
 * Created by anmpout on 04/11/2019
 */
public class ChessBoardViewModel extends AndroidViewModel {
    private LiveData<List<BoardBlock>> chessBoardBlocks;
    private MutableLiveData<Integer> chessBoardBlocksLiveData;
    private LiveData<List<Pos>> resultSet;
    private MutableLiveData<CalculationArguments> calculationArgumentsLiveData;

    public ChessBoardViewModel(@NonNull Application application) {
        super(application);
        calculationArgumentsLiveData = new MutableLiveData<>();
        chessBoardBlocksLiveData = new MutableLiveData<>();
        fetchChessBoard(8);
        resultSet = Transformations.switchMap(calculationArgumentsLiveData, input -> Transformations.map(KnightGameEngine.calculateKnightShortestPathToDest(input.start, input.end, input.dimension), result -> result));

        chessBoardBlocks = Transformations.switchMap(chessBoardBlocksLiveData, input -> Transformations.map(BoardFactory.generateChessBoard(input), input1 -> input1));
    }

    public LiveData<List<BoardBlock>> getChessBoardBlocks() {
        return chessBoardBlocks;
    }

    public LiveData<List<Pos>> getResultSet() {
        return resultSet;
    }

    public void calculateSolution(Pos start, Pos end, int dimension) {
        calculationArgumentsLiveData.setValue(new CalculationArguments(start, end, dimension));

    }

    public void fetchChessBoard(Integer dimension) {
        chessBoardBlocksLiveData.setValue(dimension);

    }

    private class CalculationArguments {
        final Pos start;
        final Pos end;
        final int dimension;

        public CalculationArguments(Pos start, Pos end, int dimension) {
            this.start = start;
            this.end = end;
            this.dimension = dimension;
        }
    }

}
