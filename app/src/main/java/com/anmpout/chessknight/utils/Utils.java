package com.anmpout.chessknight.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.lang.reflect.Method;

/**
 * Created by anmpout on 04/11/2019
 */
public class Utils {
    public static int getScreenWidth(Context context) {
        WindowManager mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display mDisplay = mWindowManager.getDefaultDisplay();
        int screenWidth;
        final DisplayMetrics metrics = new DisplayMetrics();

        screenWidth = metrics.widthPixels;
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        Method mGetRawH = null, mGetRawW = null;
        int realWidth = 0;
        // For JellyBeans and onward
        display.getRealMetrics(metrics);
        realWidth = metrics.widthPixels;
        if (realWidth > screenWidth) {
            return realWidth;
        }
        return screenWidth;
    }

    public static int dp(Context activity, int pixels) {
        float density = activity.getResources().getDisplayMetrics().density;
        return (int) (pixels * density);
    }
}
