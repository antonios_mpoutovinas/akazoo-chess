package com.anmpout.chessknight.utils;

/**
 * Created by anmpout on 02/11/2019
 */

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * The type Vertical space item decoration.
 */
public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeight;

    /**
     * Instantiates a new Vertical space item decoration.
     *
     * @param mVerticalSpaceHeight the m vertical space height
     */
    public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
        this.mVerticalSpaceHeight = mVerticalSpaceHeight;
    }

    /**
     * {@inheritDoc}
     *
     * @param outRect the out rect
     * @param view    the view
     * @param parent  the parent
     * @param state   the state
     */
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
    }
}
