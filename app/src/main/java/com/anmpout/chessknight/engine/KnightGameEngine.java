package com.anmpout.chessknight.engine;

import androidx.annotation.MainThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.anmpout.chessknight.model.Pos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by anmpout on 05/11/2019
 */
public class KnightGameEngine {
    static Pos[][] chessboard ;
    static Queue<Pos> q = new LinkedList<>();

        @MainThread
        public static LiveData<List<Pos>> calculateKnightShortestPathToDest(Pos start, Pos end, int dimension){
            if(start == null || end == null) return null;
            MutableLiveData<List<Pos>> returnList = new MutableLiveData<>();
            chessboard = new Pos[dimension][dimension];
            q = new LinkedList<>();
            populateChessBoard();
            returnList.postValue(calculateMoves(start, end));
            return returnList;
        }

    private static List<Pos> calculateMoves(Pos start, Pos end) {
        //Assign starting depth for the source as 0 (as this position is reachable in 0 moves)
        chessboard[0][0] = new Pos(start.x, start.y, 0);
        //Add start position to queue
        q.add(start);

        while (q.size() != 0) { // While queue is not empty
            Pos pos = q.poll(); //read and remove element from the queue

            //If this position is same as the end position, you found the destination
            if (end.equals(pos)) {
                // We found the Position. Now trace back from this position to get the actual shortest path
                List<Pos> path = getShortestPath(start, end);
                path.add(0,end);
                return path;
            }else {
                // perform BFS on this Pos if it is not already visited
                bfs(pos, ++pos.depth);
            }
        }
        return new ArrayList<>();
    }
      //Breadth First Search
        private static void bfs(Pos current, int depth) {

            // Start from -2 to +2 range and start marking each location on the board
            for (int i = -2; i <= 2; i++) {
                for (int j = -2; j <= 2; j++) {

                    Pos next = new Pos(current.x + i, current.y + j, depth);

                    if(inRange(next.x, next.y)) {
                        //Skip if next location is same as the location you came from in previous run
                        if(current.equals(next)) continue;

                        if (isValid(current, next)) {

                            Pos position = chessboard[next.x][next.y] ;
                            /*
                             * Get the current position object at this location on chessboard.
                             * If this location was reachable with a costlier depth, this iteration has given a shorter way to reach
                             */
                            if (position.depth > depth) {
                                chessboard[current.x + i][current.y + j] = new Pos(current.x, current.y, depth);
                                q.add(next);
                            }
                        }
                    }

                }

            }

        }

        private static boolean inRange(int x, int y) {
            return 0 <= x && x < 8 && 0 <= y && y < 8;
        }

        /*Check if this is a valid jump or position for Knight based on its current location */
        public static boolean isValid(Pos current, Pos next) {
            // Use Pythagoras theorem to ensure that a move makes a right-angled triangle with sides of 1 and 2. 1-squared + 2-squared is 5.
            int deltaR = next.x - current.x;
            int deltaC = next.y - current.y;
            return 5 == deltaR * deltaR + deltaC * deltaC;
        }

        /*Populate initial chessboard values*/
        private static void populateChessBoard() {
            for (int i = 0; i < chessboard.length; i++) {
                for (int j = 0; j < chessboard[0].length; j++) {
                    chessboard[i][j] = new Pos(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
                }
            }
        }

        /*Get the shortest Path and return iterable object */
        private static List<Pos> getShortestPath(Pos start, Pos end) {

            List<Pos> path = new ArrayList<>();

            Pos current = chessboard[end.x][end.y];
            while(! current.equals(start)) {
                path.add(current);
                current = chessboard[current.x][current.y];
            }
            path.add(new Pos(start.x, start.y, 0));
            return path;
        }

}