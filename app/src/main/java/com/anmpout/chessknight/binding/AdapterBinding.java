package com.anmpout.chessknight.binding;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.chessknight.ChessKnightApplication;
import com.anmpout.chessknight.R;
import com.anmpout.chessknight.utils.Utils;

import static com.anmpout.chessknight.model.BoardBlock.END_SELECTED;
import static com.anmpout.chessknight.model.BoardBlock.START_SELECTED;

/**
 * Created by anmpout on 17/09/2019
 */
public class AdapterBinding {

    @BindingAdapter("visibleGone")
    public static void  visibleGone(View view , Boolean isVisible) {
        if (isVisible == null) {
            return;
        }
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);

        }
    }
    @BindingAdapter("visibleInvisible")
    public static void  visibleInvisible(View view , Boolean isVisible) {
        if (isVisible == null) {
            return;
        }
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);

        }
    }

    @BindingAdapter("setTextColor")
    public static void setTextColor(TextView textView, String hex) {
        if (hex == null) {
            return;
        }
        textView.setTextColor(Color.parseColor(hex));
    }
    @BindingAdapter("dimension")
    public static void calculateBox(LinearLayout linear, Integer dimension) {
        if (dimension == null) {
            return;
        }
       int boxDimension = (int)(Utils.getScreenWidth(linear.getContext())/dimension);
        RecyclerView.LayoutParams lp =
                new RecyclerView.LayoutParams(boxDimension,boxDimension );
        linear.setLayoutParams(lp);
    }
    @BindingAdapter(value = {"row", "column","id","selected"})
    public static void giveBgColor(LinearLayout linear, Integer row, Integer column,Integer id,Integer selected) {
        if (row == null || column == null || id == null) {
            return;
        }
        if(row% 2== 1) {
            linear.setBackgroundColor(ContextCompat.getColor(ChessKnightApplication.context, id % 2 == 1 ? R.color.black : R.color.white));
        }else{
            linear.setBackgroundColor(ContextCompat.getColor(ChessKnightApplication.context, id % 2 == 1 ? R.color.white : R.color.black));
        }
        if(selected == START_SELECTED){
            linear.setBackgroundColor(ContextCompat.getColor(ChessKnightApplication.context, R.color.green));

        }else if(selected == END_SELECTED){
            linear.setBackgroundColor(ContextCompat.getColor(ChessKnightApplication.context, R.color.red));
        }
    }
}
