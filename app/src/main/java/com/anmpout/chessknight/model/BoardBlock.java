package com.anmpout.chessknight.model;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by anmpout on 04/11/2019
 */
public class BoardBlock {
    public static final int NOT_SELECTED = 0;
    public static final int START_SELECTED = 1;
    public static final int END_SELECTED = 2;
    private Integer id;
    private Integer row;
    private Integer column;

    @IntDef({NOT_SELECTED, START_SELECTED, END_SELECTED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface SelectedType {
    }

    private @SelectedType
    Integer selected;

    public BoardBlock(Integer id, Integer row, Integer column, Integer selected) {
        this.id = id;
        this.row = row;
        this.column = column;
        this.selected = selected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return row + "," + column;

    }
}
