package com.anmpout.chessknight.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anmpout on 04/11/2019
 */
public class BoardFactory {

    public static MutableLiveData<List<BoardBlock>> generateChessBoard(int dimension) {
        int totalBlocks = dimension * dimension;
        List<BoardBlock> blockList = new ArrayList<>();
        for (int i = 0; i < totalBlocks; i++) {
            int row = i / dimension;
            int column = i % dimension;
            BoardBlock tmp = new BoardBlock(i, row, column, BoardBlock.NOT_SELECTED);
            blockList.add(i, tmp);
        }
        Log.d(BoardFactory.class.getName() + " size", blockList.size() + "");
        MutableLiveData<List<BoardBlock>> returnList = new MutableLiveData<>();
        returnList.postValue(blockList);
        return returnList;
    }
}
